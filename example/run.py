from scphonons.sc_phonons import SelfConsistentPhonons
import numpy as np
import glob
from scphonons.util import merge
import ase
from phonopy.units import EvTokJmol
import hiphive

sim = SelfConsistentPhonons(
    'POSCAR',
    T=2500,
    n_iter=30,
    n_strucs=5,
    energy_eq=None,
    supercell_size=np.diag([4, 4, 4]),
    alpha=0.1,
    cutoff=7
)

sim.initial_run()
sim.run_all_iter()

at = ase.io.read('energy/vasprun.xml')
energy_eq = at.get_potential_energy()
structures_2500 = list(glob.glob('2500/structures_*.extxyz'))
merge(*structures_2500, output='structures_2500.extxyz')

max_iter = np.max([int(f.split('_')[1].split('.')[0]) for f in structures_2500])
fcm = f'2500/{max_iter}.fcm'

sim2 = SelfConsistentPhonons(
    'POSCAR',
    T=2100,
    n_iter=30,
    n_strucs=5,
    energy_eq=energy_eq,
    supercell_size=np.diag([4, 4, 4]),
    alpha=0.1,
    cutoff=7
)

sim2.continue_run('structures_2500.extxyz', fcm, 2100, 1)
structures_2100 = list(glob.glob('2100/structures_*.extxyz'))
merge(*structures_2100, *structures_2500, output='structures_2100_2500.extxyz')

max_iter = np.max([int(f.split('_')[1].split('.')[0]) for f in structures_2100])
fcm = hiphive.force_constant_model.ForceConstantModel.read(
    f'2100/{max_iter}.fcm'
)

sim3 = SelfConsistentPhonons(
    'POSCAR',
    T=2100,
    n_iter=30,
    n_strucs=5,
    energy_eq=energy_eq,
    supercell_size=np.diag([4, 4, 4]),
    alpha=0.1,
    cutoff=7
)

with open('output.dat', 'w') as outf:
    for t in np.arange(2100, 2510, 10):
        fcm, phop, corr, weff = sim3.run_reweighting_iters(
            t, fcm, 'structures_2100_2500.extxyz'
        )
        free = phop.get_thermal_properties_dict()["free_energy"][-1] / EvTokJmol
        outf.write(f'{t}\t{free}\t{corr}\t{weff}\n')
