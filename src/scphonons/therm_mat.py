from phonopy.units import *
import numpy as np
import scipy as sp


class ThermalMatrix():
    def __init__(self, phop, sc_size, T, freq_shift=0):
        self.phop = phop
        self.sc_size = sc_size
        self.T = T
        self.cov_matrix = self.build_thermal_matrix(T, freq_shift=freq_shift)
        self.samples = []
        self.set_sampler()

    def get_population(self, freq, t):
        return 1.0 / np.tanh(freq * THzToEv / (2 * Kb * t))

    def get_Q2(self, freq, t):
        return self.get_population(freq, t) / (freq * 1e12 * 2 * np.pi)

    def get_extended_eigvectors_and_eigvalues(self, phop, sc_size):
        phop.set_mesh(
            mesh=np.diag(sc_size),
            is_mesh_symmetry=False,
            is_eigenvectors=True,
            is_gamma_center=True
        )
        q, _, w, v = phop.get_mesh()
        s2u_map = phop.supercell.get_supercell_to_unitcell_map()
        u2u_map = phop.supercell.get_unitcell_to_unitcell_map()
        s2uu_map = [u2u_map[x] for x in s2u_map]
        spos = phop.supercell.get_scaled_positions()
        dim = phop.supercell.get_supercell_matrix()
        vecs = np.zeros(
            (q.shape[0], w.shape[1], len(phop.supercell.get_masses()), 3),
            dtype=complex
        )
        for q_index in range(len(q)):
            coefs = (
                np.exp(2j * np.pi * np.dot(np.dot(spos, dim.T), q[q_index]))
            )
            for band_index in range(len(w[q_index])):
                vij = v[q_index][:, band_index]
                u = []
                for k, coef in enumerate(coefs):
                    eig_index = s2uu_map[k] * 3
                    u.append(vij[eig_index:eig_index + 3] * coef)
                vecs[q_index][band_index] = np.array(u)
        return np.array(vecs), w, q

    def build_thermal_matrix(self, T, freq_shift=0):
        e_qmac, w_qm, q = self.get_extended_eigvectors_and_eigvalues(
            self.phop, self.sc_size
        )
        for q_index in range(len(q)):
            for mode_index in range(0, len(w_qm[q_index])):
                if w_qm[q_index][mode_index] <= 0:
                    print(
                        f"Imag. modes exist: Q point {q[q_index]}, Mode {mode_index} with f: {w_qm[q_index][mode_index]}, taking absolute value"
                    )
        w_qm = np.abs(w_qm)
        if freq_shift:
            print("Shifting frequencies...")
            w_qm[np.where(w_qm < 1e-1)] += freq_shift
        sqrt_masses = np.sqrt(
            [[m] * 3 for m in self.phop.supercell.get_masses()]
        )  # in sqrt(AMU), same ordering as phonopy
        mass_factor = 1. / np.outer(sqrt_masses, sqrt_masses) / AMU  # in 1/kg
        pre_factor = mass_factor / 2.  # in 1/kg
        unit_correction = Hbar * EV / Angstrom**2  # Hbar in eVs -> * EV --> Js = kgm^2/s --> /A**2 --> kg A^2/s
        pre_factor *= unit_correction  # in A^2 /s
        D = np.zeros_like(pre_factor, dtype=complex)
        for q_index in range(len(q)):
            if (q[q_index] == 0).all():
                start_ind = 3
            else:
                start_ind = 0
            for mode_index in range(start_ind, len(w_qm[q_index])):
                q2 = self.get_Q2(np.abs(w_qm[q_index][mode_index]), T)  # in s
                D += q2 * np.outer(
                    e_qmac[q_index][mode_index],
                    np.conj(e_qmac[q_index][mode_index])
                )
        D *= pre_factor / len(q)  # in A^2
        if not ((D.imag < 1e-8).all()):
            print(
                f"Imaginary parts of covariance matrix are too large\n Max: {np.max(np.abs(D.imag))}."
            )
        return D.real

    def set_sampler(self):
        self.sampler = sp.stats.multivariate_normal(
            None, cov=self.cov_matrix, allow_singular=True
        )

    def __call__(self):
        sample = self.sampler.rvs()
        self.samples.append(sample)
        return sample

    def get_logpdf(self, strucs):
        return self.sampler.logpdf(strucs)

    def __getstate__(self):
        state = self.__dict__.copy()
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
