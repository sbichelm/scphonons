import ase.io
import pathlib
from ase import Atoms, Atom
from phonopy import Phonopy
from phonopy.units import EvTokJmol
from phonopy.structure.atoms import PhonopyAtoms
from hiphive import ForceConstantPotential
from hiphive.force_constant_model import ForceConstantModel
import numpy as np
import phonopy

import numpy.linalg as la


def merge(*files, output):
    """Merges nested lists of structures into one file

    Args:
        output (string): Filename of output

    Returns:
        list: flattened list of structures
    """
    structures = []
    for fil in files:
        strucs = ase.io.read(fil, ':')
        if not isinstance(strucs[0], Atom):
            assert isinstance(strucs[0], Atoms)
            strucs = [struc for struc in strucs]
        structures.extend(strucs)
    if output:
        ase.io.write(output, structures)
    return structures


def make_input(
    uc_filename='POSCAR',
    born_filename='BORN',
    force_sets_filename='FORCE_SETS',
    mat=[4, 4, 3]
):
    """Converts phonopy generated force sets into hdf5 force constants

    Args:
        uc_filename (str, optional): Filename of input file. Defaults to 'POSCAR'.
        born_filename (str, optional): Filename of BORN file. Defaults to 'BORN'.
        force_sets_filename (str, optional): Filename of force sets. Defaults to 'FORCE_SETS'.
        mat (list, optional): diag supercell matrix. Defaults to [4, 4, 3].
    """
    phop = phonopy.load(
        supercell_matrix=mat,
        primitive_matrix="auto",
        unitcell_filename=uc_filename,
        force_sets_filename=force_sets_filename,
        born_filename=born_filename
    )
    phop.produce_force_constants()
    print(phop.force_constants.shape)
    phop.symmetrize_force_constants()
    phonopy.file_IO.write_force_constants_to_hdf5(
        phop.get_force_constants(), "fc.hdf5"
    )


def calc_kpoint_grid(atoms, kppra):
    """Return the dimensions of a MP of a given density.
                    
    Args:
        atoms: an ASE atoms object.
        kppra: approximate target number of k points per reciprocal atom.
    Returns:
        n_q: a numpy array containing the three dimensions of the grid.
    """
    n_atoms = len(atoms)
    cell = atoms.get_cell()
    lengths = np.array([la.norm(cell[i, :]) for i in range(3)])
    n_kpoints = kppra / float(n_atoms)
    lo = 0.9 * (n_kpoints * np.prod(lengths))**(1. / 3.)
    n = np.zeros(3, dtype=int)
    while np.prod(n) < n_kpoints:
        n = np.ceil(lo / lengths).astype(int)
        lo *= 1.01
    return n


def get_born(folder, phop):
    """Reads in a 'BORN' file and extracts parameters necessary for phonopy

    Args:
        folder (string): folder to look for file 'BORN'
        phop (phonopy.Phonopy): phonopy file of corresponding atoms object

    Returns:
        dict: non-analytic term correction as expected by phonopy
    """
    # Read in Born file
    path = pathlib.Path(folder) / "BORN"
    if not path.exists():
        print("BORN not found!")
        return dict()
    with path.open() as f:
        nac = phonopy.file_IO.get_born_parameters(
            f, phop.primitive, phop.get_primitive_symmetry()
        )
    nac["factor"] = phonopy.interface.calculator.get_default_physical_units(
        "vasp"
    )["nac_factor"]
    return nac


def get_delta_free_ens(phop1, phop2):
    """Calculates difference of free energy at the max temperature in two phonopy objects. 
    Note: the phonopy objects need to .run_thermal_properties run.

    Args:
        phop1 (phonopy.Phonopy): One phonopy object
        phop2 (phonopy.Phonopy): The other phonopy object

    Raises:
        Exception: Raised, if max temperatures do not align

    Returns:
        float: delta F in meV
    """
    temp_dict_1 = phop1.get_thermal_properties_dict()
    temp_dict_2 = phop2.get_thermal_properties_dict()
    if temp_dict_1["temperatures"][-1] != temp_dict_2["temperatures"][-1]:
        raise Exception("Temperatures not aligned")

    delta = 1000 * np.abs(
        temp_dict_1["free_energy"][-1] - temp_dict_2["free_energy"][-1]
    )
    delta = delta / EvTokJmol
    return delta


def fcm_to_phop(primitive, fcm_in, cs, supercell_matrix, born):
    """Converts a hiphive fcm into a phonopy object

    Args:
        primitive (ase.Atoms): Atoms object of primitive cell
        fcm_in (hiphive.force_constant_model.ForceConstantModel): Force constant model to convert
        cs (hiphive.ClusterSpace): Corresponding ClusterSpace object
        supercell_matrix (array): diagonal array of supercell matrix
        born (dict): NAC Parameters

    Returns:
        phonopy.Phonopy: phonopy obejct with force constants set
    """
    # create a phonopy object of a hiphive.force_constant_model
    fcp = ForceConstantPotential(cs, fcm_in.parameters)
    supercell, phop = build_supercell(primitive, supercell_matrix)
    fcs = fcp.get_force_constants(supercell)
    if born:
        try:
            phop.set_nac_params(born)
        except Exception as e:
            print(e)
    phop.set_force_constants(fcs.get_fc_array(order=2))
    return phop


def read_phop_hiphive(
    primitive,
    path_to_fcm,
    cluster,
    supercell_matrix,
    T,
    mesh=[51, 51, 51],
    born=None
):
    """Reads an FCM, converts it to phonopy and performs thermal properties calculations

    Args:
        primitive (ase.Atoms): Atoms object of primitive cell
        path_to_fcm (string): Path to fcm file
        cluster (hiphive.ClusterSpace): Corresponding ClusterSpace object
        supercell_matrix (array): diagonal array of supercell matrix
        T (float): max temperature for which to run thermal properties
        mesh (list, optional): q-point mesh. Defaults to [51, 51, 51].
        born (dict): NAC Parameters

    Raises:
        Exception: _description_

    Returns:
        _type_: _description_
    """
    path = pathlib.Path(path_to_fcm)
    if not (path).exists():
        raise Exception(f"FCM not found at {path}")
    fcm = ForceConstantModel.read(str(path))

    phop = fcm_to_phop(primitive, fcm, cluster, supercell_matrix, born)

    phop.auto_band_structure()
    phop.run_mesh(mesh)
    phop.run_thermal_properties(t_max=T)
    phop.run_total_dos(freq_pitch=0.1)
    return phop


def build_supercell(prim, size):
    '''Builds a supercell with phonopy type ordering of atoms in the supercell.'''
    atoms_phonopy = PhonopyAtoms(
        symbols=prim.get_chemical_symbols(),
        scaled_positions=prim.get_scaled_positions(),
        cell=prim.cell
    )
    phop = Phonopy(atoms_phonopy, supercell_matrix=size, primitive_matrix=None)
    supercell = phop.get_supercell()
    supercell = Atoms(
        cell=supercell.cell,
        numbers=supercell.numbers,
        pbc=True,
        scaled_positions=supercell.get_scaled_positions()
    )
    ase.io.write("supercell.extxyz", supercell)
    return supercell, phop