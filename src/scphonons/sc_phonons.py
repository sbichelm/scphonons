import ase.io
import pathlib
from hiphive.force_constant_model import ForceConstantModel
import numpy as np
from hiphive import StructureContainer, ClusterSpace
from hiphive.utilities import get_displacements, find_permutation, ForceConstants, extract_parameters
from hiphive.structure_generation import generate_rattled_structures
from sklearn.linear_model import LinearRegression
from ase.calculators.calculator import CalculationFailed
from sklearn.model_selection import train_test_split
import sys
import glob
from ase.calculators.vasp.vasp import Vasp

from scphonons.pulay import Pulay_mixer
from scphonons.therm_mat import ThermalMatrix
from scphonons.util import *


def init_calc():
    # Initiate Vasp calculator, gamma only, no shared mem, encut 600
    calc = Vasp(command="prun vasp_gam_noshm > vasp.out 2> vasp.err")
    calc.set(pp="PBE")
    calc.set(
        prec="Accurate",
        ediff=1e-8,
        ialgo=38,
        lplane=True,
        lscalu=False,
        ibrion=-1,
        nelmin=5,
        nelm=100,
        encut=600,
        ismear=0,
        isym=2,
        sigma=0.01,
        lreal=False,
        addgrid=True,
        lwave=False,
        lcharg=False,
        ncore=16,
        nsim=4
    )
    calc.set(kpts=[1, 1, 1], gamma=True)
    return calc


class SelfConsistentPhonons:
    """ Implement weighted self-consistent phonons
        Args:
            poscar_file - {string} path to POSCAR
            cutoff - {float} desired cutoff for hiphive.ClusterSpace
            supercell_size - {3x3 int array} desired supercell size, diag only
            threshold - {float} Convergence threshold in meV/f.u. 
            n_iter - {int} number of iteration
            n_strucs - {int} number of structures per iteration
            T - {float} temperature
            alpha - {float} Pulay mixing parameter
            energy_eq - {float} Energy of undisplaced structure
            path_to_born - {string or None} Path to BORN output of phonopy-vasp-born
    """
    def __init__(
        self,
        poscar_file,
        cutoff=7.0,
        supercell_size=np.diag([4, 4, 4]),
        threshold=2.5,
        n_iter=3,
        n_strucs=2,
        T=3000,
        alpha=0.2,
        energy_eq=-1936.23383616,
        path_to_born=''
    ):
        self.at = ase.io.read(poscar_file)
        self.threshold = threshold
        self.supercell_size = supercell_size

        if isinstance(cutoff, ClusterSpace):
            self.cs = cutoff
        else:
            self.cs = ClusterSpace(self.at, [cutoff])
            self.cs.write("cluster.cs")

        self.sc = StructureContainer(self.cs)
        self.at_ideal, phop = build_supercell(self.at, supercell_size)
        if energy_eq:
            print('Equilibrium energy read')
            self.energy_eq = energy_eq
        else:
            print('Calculating equilibrium energy')
            self.at_ideal.set_calculator(init_calc())
            self.at_ideal.calc.set(directory=f"energy")
            _, energy_eq = self._get_forces_and_energy(self.at_ideal)
            self.energy_eq = energy_eq
        self.sc_to_uc = np.fabs(1. / np.linalg.det(supercell_size))
        self.born = get_born(path_to_born, phop)
        self.n_iter = n_iter
        self.n_strucs = n_strucs
        self.T = T
        self.mesh = calc_kpoint_grid(self.at, 10000)
        self.len_params = len(self.at_ideal) * 3
        self.freq_shift = 0
        self.alpha = alpha
        self.fcm = ForceConstantModel(self.at_ideal, self.cs)
        self.pulay = Pulay_mixer(5, alpha)
        self.t_matrices = []
        self.structures = []

    def _build_tmatrix(self, fcm_in, freq_shift=0, phop=None):
        # build a thermal matrix of temp T
        if phop is None:
            phop = fcm_to_phop(
                self.at, fcm_in, self.cs, self.supercell_size, self.born
            )
        return ThermalMatrix(
            phop, self.supercell_size, self.T, freq_shift=freq_shift
        )

    def initial_run(self):
        """
        Initial rattle & initial calcs. Needs to be done only once for a particular
        poscar system and supercell size. If 0.fcm or fc.hdf5 exist, these are taken.
        """
        print(f"Initial run")
        if not (
            pathlib.Path(f"structures_0.extxyz").exists() or
            pathlib.Path("0.fcm").exists() or pathlib.Path("fc.hdf5").exists()
        ):
            print("Running the initial calculations for the first time.")
            strucs = generate_rattled_structures(
                self.at_ideal, self.n_strucs, 0.03
            )

            parsed_strucs = []
            for f, struc in enumerate(strucs):
                print(f"Structure {f}.")
                struc.set_calculator(init_calc())
                struc.calc.set(directory=f"{self.T}/0/{f}")
                forces, energy = self._get_forces_and_energy(struc)
                if energy is None:
                    continue
                parsed_struc = self._parse_structure(struc, forces, energy)
                parsed_strucs.append(parsed_struc)
                self.sc.add_structure(parsed_struc)
            ase.io.write(f"structures_0.extxyz", parsed_strucs)

            params = self._fit(init=True)

            self.fcm.parameters = params
            self.fcm.write(f"0.fcm")
            self.sc.delete_all_structures()
        else:
            if pathlib.Path("0.fcm").exists():
                print("Found a previous initial calculation")
                self.fcm = ForceConstantModel.read("0.fcm")
            elif pathlib.Path("fc.hdf5").exists():
                print("Found phonopy force constants file.")
                fcs = ForceConstants.read_phonopy(self.at_ideal, "fc.hdf5")
                print("Read FC, extracting params")
                parameters = extract_parameters(fcs, self.cs)
                print("Params extracted, setting params")
                self.fcm.parameters = parameters
                print("Finished, moving on to first iteration")
                self.fcm.write(f"0.fcm")
        self.pulay.initialize(self.fcm.parameters)
        self.t_matrices.append(self._build_tmatrix(self.fcm))

    def _fit(self, init=False):
        """Fit a weighted linear model to displacements and forces

        Args:
            init (bool, optional): Is current run initial run?. Defaults to False.

        Returns:
            array: Best fit force-constants 
        """

        weights = self.get_weights(ext=False, init=init)
        opt = LinearRegression(normalize=True, fit_intercept=False)
        X, y = self.sc.get_fit_data()
        X_train, X_test, y_train, y_test, weights_train, weights_test = train_test_split(
            X, y, weights
        )
        opt.fit(X_train, y_train, sample_weight=weights_train)
        print(
            f"Test score: {opt.score(X_test, y_test, sample_weight=weights_test)}"
        )
        best_hyper = LinearRegression(normalize=True, fit_intercept=False)
        best_hyper.fit(X, y, sample_weight=weights)
        params = best_hyper.coef_

        return params

    def get_correction(self):
        """Calculate the correction term as a weighted average of
        DFT energies and harmonic energies

        Returns:
            float: Correction
        """
        if not self.energy_eq:
            print("Eq. energy not defined. Correction unknown")
            return 0.0

        weights = self.get_weights(ext=False, rep=False)
        energies = []
        harm_energies = []
        for strucs in self.structures:
            for struc in strucs:
                energies.append(struc.arrays["energy"][0] - self.energy_eq)
                displacements = struc.arrays["displacements"]
                forces = self.fcm.get_forces(displacements)
                contrib = -np.sum(displacements * forces) / 2
                harm_energies.append(contrib)
        energies = np.array(energies)
        harm_energies = np.array(harm_energies)
        nruter = (
            np.average((energies - harm_energies), weights=weights)
        ) * self.sc_to_uc
        return nruter

    def get_weights(self, ext=True, rep=True, init=True):
        """Calculate weights of all strucs in self.structures

        Args:
            ext (bool, optional): Extend weights with ones for current iter. Defaults to True.
            rep (bool, optional): Repeat weights to be consistent with get_fit_data(). Defaults to True.
            init (bool, optional): Is current run the initial run?. Defaults to True.

        Returns:
            array: Weights
        """
        if len(self.t_matrices) < 2 and init:
            return np.ones(self.n_strucs * self.len_params)
        thermal_matrix = self.t_matrices[-1]
        weights = []
        for strucs in self.structures:
            logpdfs_old = np.array([struc.info['logpdf'] for struc in strucs])
            displacements = [
                struc.arrays['displacements'].reshape(-1) for struc in strucs
            ]
            logpdfs_new = thermal_matrix.get_logpdf(displacements)
            w = np.exp(logpdfs_new - logpdfs_old)
            if isinstance(w, float):
                w = [w]
            weights.extend(w)
        print(
            'Effective samples: ',
            self.calc_effective_samples(np.asarray(weights))
        )

        if np.all(weights == 0):
            print("All weights are zero, what are you doing?")
            weights += 1e-5
        if ext:
            weights.extend(np.ones(self.n_strucs))
        if rep:
            weights = np.repeat(weights, self.len_params)

        return np.array(weights)

    def get_effective_samples(self):
        # return effective # samples
        w = self.get_weights(ext=False, rep=False, init=False)
        return self.calc_effective_samples(w)

    def calc_effective_samples(self, w):
        # calculate effective # of samples
        return np.sum(w)**2 / np.sum(w**2)

    def _generate_phonon_rattled_structures(self, freq_shift=0.0):
        """Generates one phonon rattled structure

        Args:
            freq_shift (float, optional): Frequency shift. Defaults to 0.0

        Returns:
            ase.Atoms object
        """
        if freq_shift != self.freq_shift:
            self.t_matrices[-1] = self._build_tmatrix(
                self.fcm, freq_shift=freq_shift
            )
            self.freq_shift = freq_shift
        thermal_matrix = self.t_matrices[-1]
        at = self.at_ideal.copy()
        displ = thermal_matrix()
        logpdf = thermal_matrix.get_logpdf(displ)
        at.positions += displ.reshape(-1, 3)
        at.info["logpdf"] = logpdf
        return at

    def _generate_structures(self):
        """Generates n_strucs phonon rattled structures

        Returns:
            list of ase.Atoms
        """
        strucs = []
        fail_counter = 0
        freq_shift = 0
        self.freq_shift = 0
        while len(strucs) < self.n_strucs:
            while (True):
                if fail_counter > 10:
                    fail_counter = 0
                    freq_shift += 0.1
                    strucs = []
                try:
                    struc = self._generate_phonon_rattled_structures(freq_shift)
                    find_permutation(struc, self.at_ideal)
                    strucs.append(struc)
                    break
                except Exception as e:
                    fail_counter += 1
                    print(f"Failed generating structure: {e}, #{fail_counter}")

                if freq_shift > 1:
                    print(f"Frequency shift too large, quitting")
                    sys.exit()
        return strucs

    def _parse_structure(self, struc, forces, energy):
        """Parses the structure to be compatible with hiphive.StructureContainer

        Args:
            struc (ase.Atoms): Input structure
            forces (array): Forces of struc
            energy (float): energy of struc

        Returns:
            ase.Atoms: Structure with original positions and .arrays['displacements']
        """
        perm = find_permutation(struc, self.at_ideal)
        at_new = struc.copy()
        at_new = at_new[perm]
        at_new.arrays['forces'] = forces[perm]
        disps = get_displacements(at_new, self.at_ideal)
        at_new.arrays['energy'] = np.array([energy])
        at_new.arrays['displacements'] = disps
        at_new.positions = self.at_ideal.positions
        at_new.info['T'] = self.T
        return at_new

    def _get_forces_and_energy(self, struc):
        """Gets forces and energy of structure struc

        Args:
            struc (ase.Atoms): Input atoms object. Needs to have a calculator object that supports get_forces and get_potential_energy

        Returns:
            (array(floats), float): forces and energy
        """
        fail_counter = 0
        while True:
            if fail_counter > 4:
                print('Calculation failed')
                return None, None
            try:
                forces = struc.get_forces()
                energy = struc.get_potential_energy()
            except CalculationFailed as e:
                fail_counter += 1
                print(e)
                print(f"Calculation failed, retrying... {fail_counter}/5")
                continue
            else:
                break
        return forces, energy

    def continue_run(self, structures, fcm_in, T, start_i):
        """Continue run for given deformation at arbitrary temperature

        Args:
            structures (string): File containing structures (with logpdfs!)
            fcm (string): ForceConstantModel to start with
            T (float): Temperature in K
            start_i (int): Start index (to not overwrite existing calcs)
        """
        phop = self.set_temperature(
            T, ForceConstantModel.read(fcm_in), structures
        )
        self.run_all_iter(old_phop=phop, start_iter=start_i)

    def one_iter(self, i):
        """Run one iteration, consisting of structure generation, VASP calculation and FCM fitting

        Args:
            i (int): Current iteration
        """
        print(f"Iteration {i}.")
        strucs = self._generate_structures()

        parsed_strucs = []
        for f, struc in enumerate(strucs):
            print(f"Structure {f}.")
            struc.set_calculator(init_calc())
            struc.calc.set(directory=f"{self.T}/{i}/{f}")
            forces, energy = self._get_forces_and_energy(struc)
            if energy is None:
                continue
            parsed_struc = self._parse_structure(struc, forces, energy)
            parsed_strucs.append(parsed_struc)
            self.sc.add_structure(parsed_struc)
        ase.io.write(f"{self.T}/structures_{i}.extxyz", parsed_strucs)

        self.structures.append(parsed_strucs)
        params = self._fit()
        self.pulay.update(params)
        self.fcm.parameters = self.pulay.predict()
        self.fcm.write(f"{self.T}/{i}.fcm")
        return

    def run_all_iter(self, start_iter=1, old_phop=None):
        """Repeatedly call one_iter while continously checking for convergence

        Args:
            start_iter (int, optional): Iteration index to start from. Defaults to 1.
            old_phop (phonopy.Phonopy, optional): Phonopy object to start compare iter 1 to. Defaults to None.
        """
        if not old_phop:
            old_phop = read_phop_hiphive(
                primitive=self.at,
                path_to_fcm="0.fcm",
                cluster=self.cs,
                supercell_matrix=self.supercell_size,
                T=self.T,
                born=self.born
            )
            old_phop = self._run_phop(old_phop)
        old_params = self.fcm.parameters
        old_corr = 0
        delta_f_traj = []
        for i in range(start_iter, self.n_iter + start_iter):
            self.one_iter(i)
            new_phop = self.get_phop()
            self.t_matrices.append(self._build_tmatrix(self.fcm, phop=new_phop))
            new_corr = self.get_correction()
            delta_corr = 1000 * (old_corr - new_corr)
            delta = np.abs(get_delta_free_ens(old_phop, new_phop) + delta_corr)
            delta_f_traj.append(delta)
            print(
                f"Delta F at {self.T}K: {delta}\n \
                    Delta Param: {np.linalg.norm(old_params-self.fcm.parameters)}"
            )
            print(f"Correction term: {new_corr}.")
            if len(delta_f_traj) > 3:
                thresholds = (np.array(delta_f_traj) < self.threshold)
                if np.sum(thresholds[-3:]) == 3:
                    print("Convergence criterion reached.")
                    break
            old_corr = new_corr
            old_phop = new_phop

    def set_temperature(self, T, fcm_in, strucs):
        """Read in existing data and set-up everything so it can continue

        Args:
            T (float): Desired temperature
            fcm_in (hiphive.force_constant_model.ForceConstantModel): FCM to start from
            strucs (string): File containing all structures (obtained by merging structure_.extxyz outputs)
            
        Returns:
            phonopy.Phonopy: Initial phonopy object
        Raises:
            ValueError: Raised if structures do not have .info['logpdf']
        """
        self.t_matrices = []
        self.structures = []
        self.T = T
        structures = ase.io.read(strucs, ':')
        if not 'logpdf' in structures[0].info.keys():
            raise ValueError('Structures file has no logpdf')
        self.sc.delete_all_structures()
        for struc in structures:
            self.sc.add_structure(struc)
        self.structures.append(structures)
        self.fcm.parameters = fcm_in.parameters
        old_phop = self.get_phop()
        t_mat = self._build_tmatrix(fcm_in, phop=old_phop)
        self.t_matrices.append(t_mat)
        self.pulay = Pulay_mixer(5, self.alpha)
        self.pulay.initialize(self.fcm.parameters)
        self.t_matrices.append(t_mat)
        return old_phop

    def run_reweighting_iters(self, T, fcm_in, strucs):
        """Use all structures provided and fcm_in as starting point,
        to obtain an estimate FCM at temperature T.

        Args:
            T (float): target Temperature
            fcm_in (hiphive.force_constant_model.ForceConstantModel): Input FCM
            strucs (string): Path to structures file

        Returns:
            (Converged force constant model,
            corresponding phonopy object, 
            correction term,
            effective samples) or None
        """
        old_phop = self.set_temperature(T, fcm_in, strucs)
        old_corr = 0
        delta_f_traj = []
        while True:
            params = self._fit()
            self.pulay.update(params)
            self.fcm.parameters = self.pulay.predict()
            new_phop = self.get_phop()
            self.t_matrices.append(self._build_tmatrix(self.fcm, phop=new_phop))
            new_corr = self.get_correction()
            delta_corr = (old_corr - new_corr) * 1000
            delta = np.abs(get_delta_free_ens(old_phop, new_phop) + delta_corr)
            delta_f_traj.append(delta)
            print(f"Delta F at {self.T}K: {delta}\n")
            print(f"Correction term: {new_corr}.")
            if len(delta_f_traj) > 3:
                thresholds = (np.array(delta_f_traj) < self.threshold)
                if np.sum(thresholds[-3:]) == 3:
                    print("Convergence criterion reached.")
                    break
            if len(delta_f_traj) > 100:
                print(f'Temp {T} does not seem to converge')
                return None, None, None, None
            old_phop = new_phop
            old_corr = new_corr
        return self.fcm, new_phop, new_corr, self.get_effective_samples()

    def _run_phop(self, phop):
        """Runs all necessary phonopy calculations to extract free energies:

        Args:
            phop (phonopy.Phonopy): The phonopy object under consideration

        Returns:
            phonopy.Phonopy: The phonopy object with calculations done
        """
        phop.auto_band_structure()
        phop.run_mesh(self.mesh)
        phop.run_thermal_properties(t_max=self.T)
        phop.run_total_dos(freq_pitch=0.1)
        return phop

    def get_phop(self):
        """Turn current force constant model (self.fcm) to phonopy object

        Returns:
            phonopy.Phonopy: The phonopy object with calculations done
        """
        phop = fcm_to_phop(
            self.at, self.fcm, self.cs, self.supercell_size, self.born
        )
        return self._run_phop(phop)
