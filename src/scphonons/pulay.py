import numpy as np
import collections
import scipy as sp
import scipy.linalg as la


def calc_Pulay_coefficients(e):
    """Return the coefficients of the minimal-norm linear combination of the
    elements of "e" so that the sum of the coefficients is one.

    For more details:
    https://doi.org/10.1080%2F00268970701691611
    """
    e = np.asarray(e)
    nruter = np.empty(len(e))
    if len(e) > 1:
        e_n = e[-1]
        E = np.stack(e[:-1]).T - e_n[:, np.newaxis]
        nruter[:-1] = la.lstsq(E, -e_n, overwrite_a=True)[0]
    nruter[-1] = 1. - nruter[:-1].sum()
    return nruter


class Pulay_mixer:
    """Implement the
    direct-inversion-of-the-search-subspace algorithm for mixing,
    also known as Pulay mixing.

    See:
    Pulay, P.
    "Convergence acceleration of iterative sequences. The case of
    SCF iteration"
    Chemical Physics Letters 73 (1980) 2
    D.O.I: 10.1016/0009-2614(80)80396-4
    """
    def __init__(self, nsteps, alpha, initial = None):
        """Constructor."""
        self.initialized = False
        self.stack = collections.deque(maxlen=nsteps)
        self.residuals = collections.deque(maxlen=nsteps)
        if initial:
            self.initialized = True
            self.stack.append(initial)
            
        if alpha <= 0.0 or alpha >= 1.0:
            raise ValueError("Alpha must belong to the interval (0.0,1.0)")
        self.alpha = alpha

    def initialize(self, initial):
        """Initializer"""
        self.initialized = True
        self.stack.append(initial)

    def update(self, new):
        """Update the stored value of the density matrix based on a new pair
        of input and output.

        density matrices through a single iteration of the Pulay algorithm.
        """
        if not self.initialized:
            raise ValueError("Not initialized.")
        if new.shape != self.stack[0].shape:
            raise ValueError("The dimensions of the parameters are "
                             "incompatible with the stored values!")
        
        self.residuals.append(new-self.stack[-1])
        coeffs = calc_Pulay_coefficients(self.residuals)
        old = sum((a * v for a, v in zip(coeffs, self.stack)), np.zeros_like(new))
        self.stack.append((1. - self.alpha) * old + self.alpha * new)

    def predict(self):
        """Return the current best estimate of the density matrix."""
        if not self.initialized:
            raise ValueError("Not initialized.")
        return np.copy(self.stack[-1])
