# SCPhonons

This repository contains a cleaned-up version of the code used to obtain the data for the manuscript *Accurate first-principles treatment of the high-temperature cubic phase of hafnia*.
A typical workflow would consist of two steps:

## DFT backed runs

Run self-consistent phonon calculations of a material at a given temperature to obtain 

- an EHP at that temperature

- a set of .extxyz files containing the structures generated during the run

The latter can then be used as a starting point for further DFT backed calculations at a different temperature or for

## Reweighting without new data

The user can also choose to use the existing data and obtain EHPs at several different temperatures using a reweighting scheme.
This usually makes sense after 2 or 3 tmeperature data points have been run using DFT. 

The effective number of samples can be used as a proxy for sample relevance.

To install the Python module, run

```bash
python setup.py install
```

or

```bash
python setup.py develop
```

from the main directory.

## Ab-initio data

The repository contains an example Script (examples/run.py) and some example data using the cubic phase of HfO2 at 2500K
